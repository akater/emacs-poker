;;; -*- lexical-binding: t; -*-

(setq backup-by-copying t
      backup-directory-alist `(("." . ,(expand-file-name
                                        ".saves" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control nil)

(require 'ob-tangle)

;; this is certainly needed when org had been ripped off Emacs
;; and installed separately
(require 'org)

(require 'org-element)
(setq org-element-cache-persistent nil)

;; silence some messages
(defalias 'org-development-elisp-mode 'org-mode)

(require 'files)

(defvar use-flags nil)

(require 'cl-macs)
(require 'cl-lib)

(defun poker-make (target)
  (message "poker: making target %s" target)
  (cl-ecase target
    (all
     (poker-make 'tester)
     (poker-make 'default)
     (poker-make 'check))
    (tester
     (push 'test use-flags)
     (org-babel-tangle-file "poker-tests.org")
     (let ((default-directory (expand-file-name "build")))
       (byte-compile-file "poker-tests.el")))
    (check
     (let ((default-directory (expand-file-name "build")))
       (load (expand-file-name "poker-tests")))
     (poker-tests-run))
    (default
      (unless (memq 'test use-flags)
        (message
         "Note: necessary test dependencies are being bundled into the package"))
      (let ((files-in-order '("poker-core" "poker-range" "poker-game-state"
                              "poker-event" "poker-util" "poker-log"
                              "poker-game" "poker-buffer" "poker"
                              "poker-debug")))
        (mapc (lambda (file) (org-babel-tangle-file (concat file ".org")))
              files-in-order)
        (let ((default-directory (expand-file-name "build")))
          (mapc (lambda (file) (byte-compile-file (concat file ".el")))
                files-in-order)))))
  (message "poker: made target %s" target)
  target)
