# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: poker-range
#+subtitle: Part of the =poker= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle build/poker-range.el :mkdirp yes :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) BUG(b@/!) | DONE(d@)
#+todo: TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Require
#+begin_src emacs-lisp :results none
;; -*- lexical-binding: t; -*-

(eval-when-compile (require 'cl-macs))
(eval-when-compile (require 'cl-generic))
(eval-when-compile (require 'mmxx-macros-basic))
(eval-when-compile (require 'mmxx-macros-anaphora))
(eval-when-compile (require 'mmxx-macros-defmacro))
(require 'cl-lib)
(require 'cl-seq)
(require 'cl-extra)
(require 'poker-util)                   ; cl-plusp+
(require 'poker-core)
#+end_src

* (Abstract) Hands
** Notes
Abstract hands, as opposed to explicit hands, are equivelance classes of hands modulo different suits.  That is, abstract hands only distinguish between suited and offsuited hands.

We use full name “abstract hand” consistently in function argument names but we sometimes name local variables in let forms ~hand~ while still meaning “abstract-hand”.

** insert-abstract-hand
#+begin_src emacs-lisp :tangle no :results raw :wrap example
(with-temp-buffer
  (let ((poker-hands-imply-prefix 'o))
    (poker-core-insert-abstract-hand 'ak))
  (buffer-string))
#+end_src

#+RESULTS:
#+begin_example
AKo
#+end_example

** abstract-hand-color
*** Examples
**** Basic Examples
***** TEST-PASSED Trivial abstract hand color example
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-abstract-hand-color 'AKs)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
suited
#+end_example

*** Definition
#+begin_src emacs-lisp :results none
(defun poker-abstract-hand-color (abstract-hand)
  (let ((name (symbol-name abstract-hand)))
    (if (char-equal (aref name 0) (aref name 1)) 'paired
      (cl-ecase (aref name 2)
        (?s 'suited)
        (?o 'offsuited)))))
#+end_src

** abstract-hand-suited-p
*** Examples
**** Basic Examples
***** TEST-PASSED Trivial suited-p example
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-abstract-hand-suited-p 'AAo)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
nil
#+end_example

*** Definition
#+begin_src emacs-lisp :results none
(defun poker-abstract-hand-suited-p (abstract-hand)
  (eq 'suited (poker-abstract-hand-color abstract-hand)))
#+end_src

** do-poker-abstract-hand-names
*** Examples
**** Basic Examples
***** Macroexpansion
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(macroexpand-1
 `(do-poker-abstract-hand-names (name)
   (insert name)
   (put-text-property (- (point) 3) (point) 'face color)
   (put-text-property (- (point) 3) (point)
                      'poker-abstract-hand
                      (progn
                        (setf (aref name 2) (if suitedp ?s ?o))
                        (intern (copy-sequence name))))
   :at-end-of-row
   (delete-char -1)
   (insert ?\n)))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
(let
    ((color 'poker-hands-paired)
     suitedp
     (column 0)
     (diag 0)
     (name
      (make-string 3 32)))
  (mmxx-doproduct
      ((r1-0 poker-all-ranks)
       (r2-1 poker-all-ranks))
    (setq suitedp nil color
          (number-case column
            ((= diag)
             'poker-hands-paired)
            ((< diag)
             'poker-hands-offsuited)
            (otherwise
             (setq suitedp t)
             'poker-hands-suited)))
    (setf
     (aref name 0)
     (if suitedp r1-0 r2-1)
     (aref name 1)
     (if suitedp r2-1 r1-0)
     (aref name 2)
     32)
    (insert name)
    (put-text-property
     (-
      (point)
      3)
     (point)
     'face color)
    (put-text-property
     (-
      (point)
      3)
     (point)
     'poker-abstract-hand
     (progn
       (setf
        (aref name 2)
        (if suitedp 115 111))
       (intern
        (copy-sequence name))))
    (if-not
        (= 12 column)
        (cl-incf column)
      (delete-char -1)
      (insert 10)
      (zerof column)
      (cl-incf diag)))
  nil)
#+end_example

*** Definition
#+begin_src emacs-lisp :results none
(defmacro-mmxx do-poker-abstract-hand-names ((var &optional result)
                                             &body body
                                             &gensym r1 r2)
  (let* (at-end-of-row
         (most (aprog1 (cl-copy-tree-that-is-macro-argument body)
                 (if (eq :at-end-of-row (car it))
                     (setf at-end-of-row (cdr it)
                           it nil)
                   (do-sublists/minimal (sublist it)
                     (when (eq :at-end-of-row (cadr sublist))
                       (setf at-end-of-row (cddr sublist)
                             (cdr sublist) nil))))
                 (when (memq :at-end-of-row at-end-of-row)
                   (error "More than one `:at-end-of-row' in body %s"
                          body)))))
    `(let0 ( column diag (color 'poker-hands-paired) (suitedp)
             (,var (make-string 3 ?\s)))
       (mmxx-doproduct ((,r1 poker-all-ranks) (,r2 poker-all-ranks))
         (setq suitedp nil
               color (number-case column
                       ((= diag) 'poker-hands-paired)
                       ((< diag) 'poker-hands-offsuited)
                       (otherwise (setq suitedp t) 'poker-hands-suited)))
         (setf (aref ,var 0) (if suitedp ,r1 ,r2)
               (aref ,var 1) (if suitedp ,r2 ,r1)
               (aref ,var 2) ?\s
               ;; (aref abstract-hand-name 2) (if suitedp ?s ?\s)
               )
         ,@most
         (if-not (= 12 column) (cl-incf column)
           ,@at-end-of-row
           (zerof column)
           (cl-incf diag)))
       ,result)))
#+end_src

** poker-all-hands
#+begin_src emacs-lisp :results none
(defconst poker-all-hands
  (let (all-hands)
    (do-poker-abstract-hand-names (name (nreverse all-hands))
      (setf (aref name 2) (if suitedp ?s ?o))
      (push (intern (copy-sequence name)) all-hands))))
#+end_src

** print-s
#+begin_src emacs-lisp :results none
(defvar poker-abstract-hand-print-s nil)
#+end_src

* Range Classes
** Notes
We need to represent ranges.  So far we mostly deal with (ordered or not) sequences of (presumably uniformly distributed) abstract hands.  In general, range is a convex combination of abstract hands.

** Definition
#+begin_src emacs-lisp :results none
(defclass poker-range ()
  ()
  :documentation "Default superclass for poker ranges.")
#+end_src

#+begin_src emacs-lisp :results none
(defclass poker-range-uniform (poker-range)
  ((array :documentation "Nonempty array of uniformly distributed abstract hands, in no specific order." :initarg :array)))
#+end_src

#+begin_src emacs-lisp :results none
(defclass poker-range-uniform-relative (poker-range-uniform)
  ((array :documentation "Nonempty array of uniformly distributed abstract hands, ordered by how well they stand against a range that is the value of `relative-to' slot in the same object.")
   (relative-to :type poker-range :documentation "Source of ordering for the value of `array' slot of the same object, according to its (`array' slot's) documentation." :initarg :relative-to)))
#+end_src

* Basic Ranges
** Full range
#+begin_src emacs-lisp :results none
(defconst poker-full-range
  (make-instance 'poker-range-uniform
                 :array (cl-coerce poker-all-hands 'array)))
#+end_src

** Simplest nontrivial range
*** Definition
#+begin_src emacs-lisp :results none
(defconst poker-simplest-nontrivial-range
  (make-instance
   'poker-range-uniform-relative
   :relative-to poker-full-range
   :array [ AAo KKo QQo JJo TTo 99o 88o AKs 77o AQs AJs AKo ATs
            AQo AJo KQs 66o A9s ATo KJs A8s KTs KQo A7s A9o KJo
            55o QJs K9s A5s A6s A8o KTo QTs A4s A7o K8s A3s QJo
            K9o A5o A6o Q9s K7s JTs A2s QTo 44o A4o K6s K8o Q8s
            A3o K5s J9s Q9o JTo K7o A2o K4s Q7s K6o K3s T9s J8s
            33o Q6s Q8o K5o J9o K2s Q5s T8s K4o J7s Q4s Q7o T9o
            J8o K3o Q6o Q3s 98s T7s J6s K2o 22o Q2s Q5o J5s T8o
            J7o Q4o 97s J4s T6s J3s Q3o 98o 87s T7o J6o 96s J2s
            Q2o T5s J5o T4s 97o 86s J4o T6o 95s T3s 76s J3o 87o
            T2s 85s 96o J2o T5o 94s 75s T4o 93s 86o 65s 84s 95o
            T3o 92s 76o 74s T2o 54s 85o 64s 83s 94o 75o 82s 73s
            93o 65o 53s 63s 84o 92o 43s 74o 72s 54o 64o 52s 62s
            83o 42s 82o 73o 53o 63o 32s 43o 72o 52o 62o 42o 32o]))
#+end_src

*** Constant properties
#+begin_src emacs-lisp :results none
(defconst poker-simplest-nontrivial-range-array
  (oref poker-simplest-nontrivial-range array))
#+end_src

#+begin_src emacs-lisp :results none
(defconst poker-simplest-nontrivial-range-array-length
  (length poker-simplest-nontrivial-range-array))
#+end_src

#+begin_src emacs-lisp :results none
(defconst poker-simplest-nontrivial-range-array-max-position
  (1- poker-simplest-nontrivial-range-array-length))
(defconst poker-simplest-nontrivial-range-array-min-position
  0)
#+end_src

* Simple Subranges
** simple-uniform-subrange
*** Definition
#+begin_src emacs-lisp :results none
(defclass poker-simple-uniform-subrange (poker-range)
  ((parent :type poker-range :initarg :parent)
   (length :type integer :documentation "Must be nonnegative, less or equal to total number of (non-zero probability) hands in `parent'." :reader poker-subrange-length :writer set-poker-subrange-length
           ;; we might actually want to have initform 0 for length.  Later.
           ;; 
           ;; shit
           :accessor poker-subrange-olength
           :initarg :length)
   (presentations :type list :initform nil
    ;; this slot better be inherited from a specialized class
    ;; but we don't have that so far
    :documentation "Whenever a presentation of an object is created (displayed, inserted into a buffer), some reference to it should be put into this list.  Presentation itself should contain a reference to the object.  Thus, the link is bidirectional."))
  :documentation "Range consisting of n first hands of `parent' range, uniformly distributed.")
#+end_src

When length is updated, all dependent widgets should be updated.

Ideally, a method like this should be automatically defined on initialization each instance with slot presentations.
#+begin_src emacs-lisp :results none
(cl-defmethod set-poker-subrange-length :after (obj new-value)
  (with-akater-ellipsis-done-message "Updating presentations"
    (mapc #'adhoc-update-presentation (slot-value obj 'presentations))))
#+end_src
The method may very well be rewritten eventually but it's good to define automatically for convenience.

#+begin_src emacs-lisp :results none
(defun adhoc-update-presentation (presentation)
  (cl-etypecase presentation
    (function (funcall presentation))))
#+end_src

#+begin_src emacs-lisp :tangle no :results none :eval never
(defun adhoc-update-presentation (buffer-and-point)
  (cl-destructuring-bind (buffer . point) buffer-and-point
    (with-current-buffer buffer
      (goto-char point)
      (method-update-presentation))))
#+end_src

*** TODO define a before method for presentations slot setter that would check if presentations are being removed from the slot
- State "TODO"       from              [2020-12-16 Wed 11:50]
** hand-is-in-simple-subrange-p
*** Examples
**** Basic Examples
***** TEST-PASSED 99o is the last one among the first 6 of the simlpest non-trivial range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-hand-is-in-simple-subrange-p '99o
                                    (poker-make-simple-preflop-range
                                     :length 6))
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
5
#+end_example

***** TEST-PASSED AKs is not among the first 6 of the simlpest non-trivial range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-hand-is-in-simple-subrange-p 'AKs
                                    (poker-make-simple-preflop-range
                                     :length 6))
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
nil
#+end_example

*** Definition
#+begin_src emacs-lisp :results none
(defun poker-hand-is-in-simple-subrange-p (abstract-hand range)
  (cl-position abstract-hand (oref (oref range parent) array)
               :test #'eq :end (oref range length)))
#+end_src

* make-simple-preflop-range
** Examples
*** Basic Examples
**** Create, set length
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(let ((test-range (poker-make-simple-preflop-range)))
  (setf (oref test-range length) 1)
  ;; (setf (poker-subrange-length test-range) 1)
  ;; (setf (poker-subrange-olength test-range) 2)
  (set-poker-subrange-length test-range 2)
  ;; (set-poker-subrange-length test-range 2)
  (oref test-range length)
  )
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
2
#+end_example

**** Initialize length
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(let ((test-range (poker-make-simple-preflop-range :length 6)))
  (oref test-range length))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
6
#+end_example

** Definition
#+begin_src emacs-lisp :results none
(defun poker-make-simple-preflop-range (&rest properties)
  "PROPERTIES is passed to `make-instance'."
  (apply #'make-instance 'poker-simple-uniform-subrange
         :parent poker-simplest-nontrivial-range
         properties))
#+end_src

* Picker
** Notes
Range representation is a table along the lines of
#+begin_src emacs-lisp :tangle no :results code :wrap xample emacs-lisp
( -partition 13
  (let ((s (make-string 3 0)))
    (map-product (lambda (r1 r2) (if (poker-rank-< r2 r1)
                                     (setf (aref s 0) r1
                                           (aref s 1) r2
                                           (aref s 2) ?s)
                                   (setf (aref s 0) r2
                                         (aref s 1) r1
                                         (aref s 2) ?\s))
                   (copy-sequence s))
                 poker-all-ranks poker-all-ranks)))

#+end_src

#+RESULTS:
#+begin_example emacs-lisp
(("AA " "AKs" "AQs" "AJs" "ATs" "A9s" "A8s" "A7s" "A6s" "A5s" "A4s" "A3s" "A2s")
 ("AK " "KK " "KQs" "JK " "KTs" "K9s" "K8s" "K7s" "K6s" "K5s" "K4s" "K3s" "K2s")
 ("AQ " "KQ " "QQ " "JQ " "QTs" "Q9s" "Q8s" "Q7s" "Q6s" "Q5s" "Q4s" "Q3s" "Q2s")
 ("AJ " "JKs" "JQs" "JJ " "JTs" "J9s" "J8s" "J7s" "J6s" "J5s" "J4s" "J3s" "J2s")
 ("AT " "KT " "QT " "JT " "TT " "T9s" "T8s" "T7s" "T6s" "T5s" "T4s" "T3s" "T2s")
 ("A9 " "K9 " "Q9 " "J9 " "T9 " "99 " "98s" "97s" "96s" "95s" "94s" "93s" "92s")
 ("A8 " "K8 " "Q8 " "J8 " "T8 " "98 " "88 " "87s" "86s" "85s" "84s" "83s" "82s")
 ("A7 " "K7 " "Q7 " "J7 " "T7 " "97 " "87 " "77 " "76s" "75s" "74s" "73s" "72s")
 ("A6 " "K6 " "Q6 " "J6 " "T6 " "96 " "86 " "76 " "66 " "65s" "64s" "63s" "62s")
 ("A5 " "K5 " "Q5 " "J5 " "T5 " "95 " "85 " "75 " "65 " "55 " "54s" "53s" "52s")
 ("A4 " "K4 " "Q4 " "J4 " "T4 " "94 " "84 " "74 " "64 " "54 " "44 " "43s" "42s")
 ("A3 " "K3 " "Q3 " "J3 " "T3 " "93 " "83 " "73 " "63 " "53 " "43 " "33 " "32s")
 ("A2 " "K2 " "Q2 " "J2 " "T2 " "92 " "82 " "72 " "62 " "52 " "42 " "32 " "22 "))
#+end_example

Emacs does not seem to offer proper interface to dealing with tables or grids in a buffer.

We provide some ad hoc solution to this.  Picker is a kind of widget that allows to inspect and modify range (or maybe another Lisp object) parameters.

We only implement textual picker for objects of class ~poker-uniform-subrange~.

** (hands) Navigation
*** Notes
One day this will go to the section [[(Abstract) Hands]].

*** abstract-hand-at
We do not attempt to distinguish by analyzing text only.  It might be worthwhile in theory but at the moment we use text properties consistently and plan to do so in future.
#+begin_src emacs-lisp :results none
(defun poker-abstract-hand-at (&optional point)
  (get-text-property (or point (point)) 'poker-abstract-hand))
#+end_src

*** abstract-hand-at-point
#+begin_src emacs-lisp :results none
(defun poker-abstract-hand-at-point ()
  (poker-abstract-hand-at (point)))
#+end_src

*** forward-abstract-hand
#+begin_src emacs-lisp :results none
(defun poker-forward-abstract-hand (&optional n)
  "Moves charwise, not taking range pickers layout into account."
  (interactive "p")
  (setq n (or n 1))
  (if-not (cl-plusp n) (poker-backward-abstract-hand (- n))
    (let (hand-to-skip)
      (cl-loop repeat n
               do (if (setq hand-to-skip (poker-abstract-hand-at-point))
                      (while (aand (poker-abstract-hand-at-point)
                                   (eq hand-to-skip it))
                        (forward-char))
                    (until (setq hand-to-skip (poker-abstract-hand-at-point))
                      (forward-char))
                    (while (aand (poker-abstract-hand-at-point)
                                 (eq hand-to-skip it))
                      (forward-char))))
      (poker-abstract-hand-at-point))))
#+end_src

*** backward-abstract-hand
#+begin_src emacs-lisp :results none
(defun poker-backward-abstract-hand (&optional n)
  "Moves charwise, not taking range pickers layout into account."
  (interactive "p")
  (setq n (or n 1))
  (if-not (cl-plusp n) (poker-forward-abstract-hand (- n))
    (let (hand-to-skip)
      (cl-loop repeat n
               do (if (setq hand-to-skip (poker-abstract-hand-at-point))
                      (while (aand (poker-abstract-hand-at-point)
                                   (eq hand-to-skip it))
                        (backward-char))
                    (until (setq hand-to-skip (poker-abstract-hand-at-point))
                      (backward-char))
                    (while (aand (poker-abstract-hand-at-point)
                                 (eq hand-to-skip it))
                      (backward-char))))
      (poker-abstract-hand-at-point))))
#+end_src

** picker-insert
*** Examples
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(with-current-buffer "*poker-range*"
  (poker-range-picker-insert (poker-make-simple-preflop-range :length 3)))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
nil
#+end_example

*** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-picker-insert (range)
  ;; here, we could actually display only hands from parent array
  (let (marker)
    (prog1
        (progn
          (setq marker (point-marker))
          (insert-rectangle 
           (let ((column 0) (diag 0) (hand-name (make-string 3 ?\s))
                 suitedp hi lo table)
             (with-temp-buffer
               (erase-buffer)
               (dolist (hand poker-all-hands)
                 (setq hand-name (symbol-name hand)
                       hi (aref hand-name 0)
                       lo (aref hand-name 1)
                       suitedp (char-equal ?s (aref hand-name 2)))
                 (insert hi lo (if (and suitedp poker-abstract-hand-print-s) ?s
                                 ?\s))
                 (put-text-property (- (point) 3) (point)
                                    'face (cond
                                           (suitedp 'poker-hands-suited)
                                           ((char-equal hi lo) 'poker-hands-paired)
                                           (t 'poker-hands-offsuited)))
                 (put-text-property (- (point) 3) (point) 'poker-abstract-hand hand)
                 (put-text-property (- (point) 3) (point) 'poker-range-picker range)
                 (if-not (= 12 column) (cl-incf column)
                   (unless poker-abstract-hand-print-s (delete-char -1))
                   (push (buffer-string) table)
                   (erase-buffer)
                   (zerof column)
                   (cl-incf diag))))
             (nreverse table))))
      ;; todo: bring it to initial state
      ;; ideally, this should happen after the following push
      ;; but eieio is too dumb for that
      (push (lambda ()
              (with-current-buffer (marker-buffer marker)
                (save-excursion
                  (goto-char marker)
                  (poker-range-picker-at-point-update))))
            (oref range presentations)))))
#+end_src

** picker-top-left
*** Definition
See also [[file:poker-core.org::*visualization-top-left]]
#+begin_src emacs-lisp :results none
(defun poker-range-picker-top-left ()
  (if-not (get-text-property (point) 'poker-range-picker)
      (error "Not in a picker")
    (while (aand (cl-plusp+ (1- (point)))
                 (get-text-property it 'poker-range-picker))
      (backward-char))
    (let ((point-above (point)))
      (while (save-excursion
               (unless (= (point) (point-min))
                 (next-logical-line -1)
                 (get-text-property (setq point-above (point))
                                    'poker-range-picker)))
        (goto-char point-above))
      point-above)))
#+end_src

** picker-at-point-update
*** Examples
**** likely deprecated example unfit for ~range-picker-at-point-update~
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(with-current-buffer "*poker-range*"
  (poker-range-update-highlight-above 'T9s))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
nil
#+end_example

*** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-picker-at-point-update ()
  (let ((range (or (get-text-property (point) 'poker-range-picker)
                   (error "Not in the poker range picker.")))
        buffer-read-only)
    (save-excursion
      (let ((picker-top-left (poker-range-picker-top-left)))
        (goto-char picker-top-left)
        (let0 (column (row 1) (height 13) (hand))
          (cl-block nil
            (while (setq hand (get-text-property (point) 'poker-abstract-hand))
              (put-text-property
               (point) (+ 2 (point))
               'face (if (poker-hand-is-in-simple-subrange-p hand range)
                         (cl-case (poker-abstract-hand-color hand)
                           (suited 'poker-hands-suited-highlighted)
                           (offsuited 'poker-hands-offsuited-highlighted)
                           (paired 'poker-hands-paired-highlighted))
                       (cl-case (poker-abstract-hand-color hand)
                         (suited 'poker-hands-suited)
                         (offsuited 'poker-hands-offsuited)
                         (paired 'poker-hands-paired))))
              (if-not (= 12 column) (progn (poker-forward-abstract-hand)
                                           (cl-incf column))
                (goto-char picker-top-left)
                (if (= row height) (cl-return)
                  (next-logical-line row)
                  (zerof column)
                  (cl-incf row))))))))))
#+end_src

** Pick
*** range-pick-downto-hand-at-point
**** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-pick-downto-hand-at-point ()
  (interactive)
  (let ((range (or (get-text-property (point) 'poker-range-picker)
                   (error "Not at poker range picker.")))
        (hand (get-text-property (point) 'poker-abstract-hand)))
    (set-poker-subrange-length
     range
     (1+ (or (cl-position hand (oref (oref range parent) array)
                          :test #'eq)
             (error "Hand %s is not in parent range of %s" hand range))))))
#+end_src

*** range-pick-downto-next-in-parent
**** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-pick-downto-next-in-parent (&optional n)
  "Within standard diagonal picker"
  (interactive "p")
  (setq n (or n 1))
  (let* ((range (get-text-property (point) 'poker-range-picker))
         (hand-position (1- (oref range length))))
    ;; this whole thing below should be in a defmethod (setf length)
    ;; while here we'd simply (cl-incf (length range) n) instead
    ;; but eieio is too dumb for it
    (let*0 ((parent-array (oref (oref range parent) array))
            (parent-array-max-position (1- (length parent-array)))
            parent-array-min-position)
      (cond
       ((zerop n))
       ((and (cl-plusp n)
             (= hand-position parent-array-max-position))
        (message "This is the last hand in range."))
       ((and (cl-minusp n)
             (= hand-position parent-array-min-position))
        (message "This is the first hand in range."))
       (t (let ((next-hand-position (+ n hand-position)))
            (if-not (<= parent-array-min-position
                        next-hand-position
                        parent-array-max-position)
                (error "%s hands %s is out of range."
                       (abs n) (if (cl-plusp n) 'forward 'backward))
              (set-poker-subrange-length range (1+ next-hand-position))
              ;; todo: message: Downto T9s (37%)
              )))))))
(defalias 'poker-range-trivial-next 'poker-range-pick-downto-next-in-parent)
#+end_src

*** range-pick-downto-prev-in-parent
**** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-pick-downto-prev-in-parent (&optional n)
  "Within standard diagonal picker"
  (interactive "p")
  (poker-range-pick-downto-next-in-parent (- n)))
(defalias 'poker-range-trivial-prev 'poker-range-pick-downto-prev-in-parent)
#+end_src

** Percentages
*** range-as-number
**** Examples
***** Basic Examples
****** TEST-PASSED Q9s is quarter range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-as-number 'Q9s)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
0.2485207100591716
#+end_example

****** TEST-PASSED 95s is 2/3 range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-as-number '95s)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
0.6627218934911243
#+end_example

**** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-as-number (hand)
  (/ (float (cl-position hand poker-simplest-nontrivial-range-array))
     poker-simplest-nontrivial-range-array-length))
#+end_src

*** number-to-hand
**** Examples
***** Basic Examples
****** TEST-PASSED 23% range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-number-to-hand 0.23)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
A3s
#+end_example

****** TEST-PASSED 38% range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-number-to-hand 0.38)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
T9s
#+end_example

****** TEST-PASSED 67% range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-number-to-hand 0.67)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
95s
#+end_example

***** Possible Issues
****** TEST-PASSED Can return a hand without error for more than 100%
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-number-to-hand 1.00591)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
32o
#+end_example

**** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-number-to-hand (p)
  (awhen (cl-plusp+
          (1- (floor (* p poker-simplest-nontrivial-range-array-length))))
    (aref poker-simplest-nontrivial-range-array it)))
#+end_src

**** Tests
***** TEST-PASSED 0% range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-number-to-hand 0)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
nil
#+end_example

***** TEST-PASSED 100% range
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-number-to-hand 1)
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
32o
#+end_example

*** default-number-range-percents
#+begin_src emacs-lisp :results none
(defvar poker-default-number-range-percents 20)
#+end_src

*** goto-by-number
**** Examples
***** Basic Examples
****** TEST-PASSED Goto 25% range by number
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(with-current-buffer "*poker-range*"
  (poker-range-goto-by-number 25))
#+end_src

#+EXPECTED:
#+begin_example emacs-lisp
nil
#+end_example

**** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-goto-by-number (&optional percents)
  (interactive "p")
  (let0 ( column diag
          (hand (poker-range-number-to-hand
                 (/ (or percents poker-default-number-range-percents) 100.0))))
    (goto-char (point-min))
    (until (eq hand (get-text-property (point) 'poker-abstract-hand))
      (if-not (= 12 column) (progn (forward-char 3) (cl-incf column))
        (beginning-of-line) (ignore-errors (next-logical-line))
        (zerof column)
        (cl-incf diag)))
    (poker-range-pick-downto-hand-at-point)))
#+end_src

** TODO avy-goto-and-pick-poker-range-hand
- State "TODO"       from              [2020-12-16 Wed 14:18] \\
  Display shortcuts to hands in pickers only, immediately pick after jump.
  Also allow to jump by native chars (this will require showing labels if there's more than one picker on screen).
  Also allow to select without jumping.
* trying to make it right (high-level)
** We need some kind of iterator
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(with-current-buffer "*poker-range*"
  (save-excursion
    (goto-char (point-min))
    (let ((column 0) (diag 0) (abstract-hand (make-string 3 ?\s))
          hand hands)
      (while (setq hand (get-text-property (point) 'poker-abstract-hand))
        (push hand hands)
        ;; (put-text-property (- (point) 3) (point) 'face color)
        ;; (put-text-property (- (point) 3) (point)
        ;;                    'poker-abstract-hand
        ;;                    (progn
        ;;                      (setf (aref abstract-hand 2) (if suitedp ?s ?o))
        ;;                      (intern (copy-sequence abstract-hand))))
    
        (if-not (= 12 column) (progn (forward-char 3) (cl-incf column))
          (beginning-of-line) (ignore-errors (next-logical-line))
          (zerof column)
          (cl-incf diag)))
      (nreverse hands))))
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
(AAo AKs AQs AJs ATs A9s A8s A7s A6s A5s A4s A3s A2s AKo KKo KQs KJs KTs K9s K8s K7s K6s K5s K4s K3s K2s AQo KQo QQo QJs QTs Q9s Q8s Q7s Q6s Q5s Q4s Q3s Q2s AJo KJo QJo JJo JTs J9s J8s J7s J6s J5s J4s J3s J2s ATo KTo QTo JTo TTo T9s T8s T7s T6s T5s T4s T3s T2s A9o K9o Q9o J9o T9o 99o 98s 97s 96s 95s 94s 93s 92s A8o K8o Q8o J8o T8o 98o 88o 87s 86s 85s 84s 83s 82s A7o K7o Q7o J7o T7o 97o 87o 77o 76s 75s 74s 73s 72s A6o K6o Q6o J6o T6o 96o 86o 76o 66o 65s 64s 63s 62s A5o K5o Q5o J5o T5o 95o 85o 75o 65o 55o 54s 53s 52s A4o K4o Q4o J4o T4o 94o 84o 74o 64o 54o 44o 43s 42s A3o K3o Q3o J3o T3o 93o 83o 73o 63o 53o 43o 33o 32s A2o K2o Q2o J2o T2o 92o 82o 72o 62o 52o 42o 32o 22o)
#+end_example

** OBSOLETE in terms of iterator
- State "OBSOLETE"   from              [2020-12-15 Tue 11:54]
#+begin_src emacs-lisp :tangle no :results none :eval never
(defun poker-range-insert ()
  (with-current-buffer "*poker-range*"
    (erase-buffer)
    (let0 (column diag)
      (do-poker-abstract-hand-names (name)
        (insert name)
        (put-text-property (- (point) 3) (point) 'face color)
        (put-text-property (- (point) 3) (point)
                           'poker-abstract-hand
                           (progn
                             (setf (aref name 2) (if suitedp ?s ?o))
                             (intern (copy-sequence name))))
        (if-not (= 12 column) (cl-incf column)
          (delete-char -1)
          (insert ?\n)
          (zerof column)
          (cl-incf diag))))))
#+end_src

** range-insert in terms of iterator
#+begin_src emacs-lisp :results none
(defun poker-range-insert ()
  (with-current-buffer "*poker-range*"
    (erase-buffer)
    (do-poker-abstract-hand-names (name)
      (insert name)
      (put-text-property (- (point) 3) (point) 'face color)
      (put-text-property (- (point) 3) (point)
                         'poker-abstract-hand
                         (progn
                           (setf (aref name 2) (if suitedp ?s ?o))
                           (intern (copy-sequence name))))
      :at-end-of-row
      (delete-char -1)
      (insert ?\n))))
#+end_src

** do-poker-abstract-hands
#+begin_src emacs-lisp :results none
(cl-defmacro do-poker-abstract-hands ((var) &rest body)
  (declare (indent 1))
  `(dolist (,var poker-all-hands)
     ,@body))
#+end_src

** insert in terms of nonexistent iterator
how we'd like it to go
#+begin_src emacs-lisp :tangle no :results none :eval never
(defun poker-range-insert ()
  (with-current-buffer "*poker-range*"
    (erase-buffer)
    (do-poker-abstract-hands (hand)
      (setf (aref poker-hand-name 2) (if suitedp ?s ?\s))
      (intern (copy-sequence poker-hand-name)
      (insert poker-hand-name)
      (put-text-property (- (point) 3) (point) 'face color)
      (put-text-property (- (point) 3) (point) 'poker-abstract-hand hand)
      :at-end-of-row
      (delete-char -1)
      (insert ?\n)))))
#+end_src

#+begin_src emacs-lisp :tangle no :results none :eval never
(defun poker-range-insert ()
  (with-current-buffer "*poker-range*"
    (erase-buffer)
    (do-poker-range-region ( :buffer poker-range-buffer)
      (setf (aref poker-hand-name 2) (if suitedp ?s ?\s))
      (intern (copy-sequence poker-hand-name)
      (insert poker-hand-name)
      (put-text-property (- (point) 3) (point) 'face color)
      (put-text-property (- (point) 3) (point) 'poker-abstract-hand hand)
      :at-end-of-row
      (delete-char -1)
      (insert ?\n)))))
#+end_src

* poker-range-mode
#+begin_src emacs-lisp :results none
;;;###autoload
(define-derived-mode poker-range-mode fundamental-mode "Poker-range"
  "Displaying poker hand ranges."
  (setq buffer-read-only t
        buffer-undo-list t)
  (setq-local poker-range-mode t))
#+end_src

* Some keybindings
#+begin_src emacs-lisp :results none
(define-key poker-range-mode-map "n"
  #'poker-range-pick-downto-next-in-parent)
(define-key poker-range-mode-map "p"
  #'poker-range-pick-downto-prev-in-parent)
(define-key poker-range-mode-map "g"
  #'poker-range-pick-downto-hand-at-point)
(define-key poker-range-mode-map "f"
  #'poker-range-goto-by-number)
(define-key poker-range-mode-map [?0] 'universal-argument)
(define-key poker-range-mode-map [?1] 'digit-argument)
(define-key poker-range-mode-map [?2] 'digit-argument)
(define-key poker-range-mode-map [?3] 'digit-argument)
(define-key poker-range-mode-map [?4] 'digit-argument)
(define-key poker-range-mode-map [?5] 'digit-argument)
(define-key poker-range-mode-map [?6] 'digit-argument)
(define-key poker-range-mode-map [?7] 'digit-argument)
(define-key poker-range-mode-map [?8] 'digit-argument)
(define-key poker-range-mode-map [?9] 'digit-argument)
#+end_src

* display-new-buffer
** Examples
#+begin_src emacs-lisp :tangle no :results code :wrap example emacs-lisp
(poker-range-display-new-buffer "*poker-range*")
#+end_src

#+RESULTS:
#+begin_example emacs-lisp
#<buffer *poker-range*>
#+end_example

** Definition
#+begin_src emacs-lisp :results none
(defun poker-range-display-new-buffer (&optional buffer-or-name)
  (interactive)
  (setq buffer-or-name (or buffer-or-name "*poker-range*"))
  (with-current-buffer (get-buffer-create buffer-or-name)
    (display-buffer (current-buffer))
    (poker-range-mode)
    (let (buffer-read-only)
      (erase-buffer)
      (poker-range-picker-insert (poker-make-simple-preflop-range :length 1)))
    (goto-char (point-min))
    (current-buffer)))
#+end_src

* Provide
#+begin_src emacs-lisp :results none
(provide 'poker-range)

;;; poker-range.el ends here
#+end_src
