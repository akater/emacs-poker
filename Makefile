# makefile for Elisp poker package

# Author: Dima Akater

# Emacs invocation
EMACS_COMMAND   := emacs

EMACS		:= $(EMACS_COMMAND) -L lisp --batch --no-site-file

EMACS_INIT_EVAL := $(EMACS) --load init.el --eval

default:
	$(EMACS) --load init.el --load make.el

.PHONY: clean all

all:
	$(EMACS_INIT_EVAL) "(poker-make 'all)"

check:
	$(EMACS_INIT_EVAL) "(poker-make 'check)"

clean:
	rm -rf build

DESTDIR=
SITELISP=${DESTDIR}/usr/share/emacs/site-lisp/
LISPDIR=${SITELISP}poker/
DOCSDIR=${DESTDIR}/usr/share/doc/poker
install:
	test -d ${LISPDIR} || mkdir -p ${LISPDIR}
	rm -f ${LISPDIR}*.el
	rm -f ${LISPDIR}*.elc
	cp -f build/*.el ${LISPDIR}
	cp -f build/*.elc ${LISPDIR}
	cp README.org ${DOCSDIR}
uninstall:
	rm -vf ${LISPDIR}*.el
	rm -vf ${LISPDIR}*.elc
